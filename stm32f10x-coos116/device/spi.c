/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "spi.h"
/* 定义通用数据结构 */
typedef enum
{
    SPI_BUS_0=0,
    SPI_BUS_1,
    SPI_BUS_2,
    SPI_BUS_MAX,
    SPI_BUS_NA=0xFF
}SPI_BUS_E;

typedef struct{
    uint32 m_gpioMOSI;
    uint32 m_gpioMISO;
    uint32 m_gpioSCK;
    uint8 m_currDev;
    sint8 m_mutex;
    uint8 m_initFlag;
}SpiBus_S;

typedef struct{
    uint8 m_devId;
    uint8 m_spiBus;
    uint8 m_enCS;
    uint32 m_gpioCS;
}SpiDev_S;

#if USE_SPI_BASED_ON_GPIO
#include "spi_gpio.c"
#else
#include "gpio.h"
#include "board.h"
#include "trace.h"
#include "kernel.h"
#include "mymutex.h"


#define SPI_RW_TIMEOUT    300

static SpiBus_S s_spiBus[SPI_BUS_MAX]={
  /* MOSI  MISO  SCK   currentDev  mutex  initFlag */
    {PA(7),PA(6),PA(5),SPI_DEV_NA  ,-1    ,0},  /* SPI_BUS_0 */
    {0    ,0    ,0    ,SPI_DEV_NA  ,-1    ,0},  /* SPI_BUS_1 */
    {0    ,0    ,0    ,SPI_DEV_NA  ,-1    ,0}   /* SPI_BUS_2 */
};

static SpiDev_S s_spiDev[SPI_DEV_MAX]={
    {SPI_DEV_PGA2311_1,SPI_BUS_0,0,PA(4)},
    {SPI_DEV_PGA2311_2,SPI_BUS_1,0,PA(3)}
};

SPI_TypeDef* get_spi(uint8 spi_bus)
{
    switch(spi_bus){
    case SPI_BUS_0:
        if(!s_spiBus[spi_bus].m_initFlag)
        {
            RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
        }
        return SPI1;
    case SPI_BUS_1:
        if(!s_spiBus[spi_bus].m_initFlag)
        {
            RCC_APB2PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
        }
        return SPI2;
    case SPI_BUS_2:
        if(!s_spiBus[spi_bus].m_initFlag)
        {
            RCC_APB2PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);
        }
        return SPI3;
    default:
        break;
    }
    return NULL;
}

static bool spi_soc_init(uint8 spi_bus)
{ 
    if (spi_bus>=SPI_BUS_MAX)
    {
        TRACE_ERR("Invalid spi bus:%d!",spi_bus);
        return false;
    }
    if (s_spiBus[spi_bus].m_initFlag)
    {
        return true;
    }
    switch(spi_bus){
    case SPI_BUS_0:
    {
        SPI_InitTypeDef  SPI_InitStructure; 
        GPIO_INIT(s_spiBus[spi_bus].m_gpioSCK, GPIO_MODE_AF_PP, GPIO_SPEED_50MHZ);
        GPIO_INIT(s_spiBus[spi_bus].m_gpioMISO, GPIO_MODE_AF_PP, GPIO_SPEED_50MHZ);
        GPIO_INIT(s_spiBus[spi_bus].m_gpioMOSI, GPIO_MODE_AF_PP, GPIO_SPEED_50MHZ);
        SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;/* 双线双向全双工 */
        SPI_InitStructure.SPI_Mode = SPI_Mode_Master;       /* SPI主模式 */
        SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;   /* 发送接收8位帧结构 */
        SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;         /* 选择串行时钟悬空高 */
        SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;        /* 数据捕获于第二个时钟沿 */
        SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;           /* NSS信号控制 */
        SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;/* 波特率预分频值 */
        SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;  /* 数据传输从MSB位开始 */
        SPI_InitStructure.SPI_CRCPolynomial = 7;            /* CRC值计算的多项式 */
        SPI_Init(get_spi(spi_bus), &SPI_InitStructure);                 /* SPI初始化 */
        SPI_Cmd(get_spi(spi_bus), ENABLE);                              /* 使能SPI外设 */
        break;
    }
    default:
        TRACE_ERR("init invalid spi bus:%d",spi_bus);
        return false;
    }
    s_spiBus[spi_bus].m_initFlag=true;
    TRACE_REL("init spi_bus ok.");
    return true;
}

static bool spi_soc_set_dev(uint8 spi_dev, bool enable)
{
    uint8 spi_bus;
    if(spi_dev>=SPI_DEV_MAX)
    {
        return false;
    }
    spi_bus = s_spiDev[spi_dev].m_spiBus;
    if(s_spiBus[spi_bus].m_initFlag)
    {
        GPIO_INIT(s_spiDev[spi_dev].m_gpioCS,GPIO_MODE_AF_PP,GPIO_Speed_50MHz);
        if(enable)
        {
            GPIO_SET_VAL(s_spiDev[spi_dev].m_gpioCS, s_spiDev[spi_dev].m_enCS);
        }
        else
        {
            GPIO_SET_VAL(s_spiDev[spi_dev].m_gpioCS, !(s_spiDev[spi_dev].m_enCS));
        }
        return true;
    }
    return false;
}

bool spi_soc_open(uint8 spi_dev)
{
    uint8 spi_bus;
    if(spi_dev>=SPI_DEV_MAX)
    {
        return false;
    }
    spi_bus = s_spiDev[spi_dev].m_spiBus;
    if(!spi_soc_init(spi_bus))
    {
        return false;
    }
    mutex_lock(s_spiBus[spi_bus].m_mutex);
    if(!spi_soc_set_dev(spi_dev, true))
    {
        return false;
    }
    s_spiBus[spi_bus].m_currDev = spi_dev;
    return true;
}

bool spi_soc_close(uint8 spi_dev)
{
    uint8 spi_bus;
    if(spi_dev>=SPI_DEV_MAX)
    {
        return false;
    }
    spi_bus = s_spiDev[spi_dev].m_spiBus;
    if(s_spiBus[spi_bus].m_currDev!=spi_dev)
    {
        return false;
    }
    if (spi_soc_set_dev(spi_dev, false))
    {
        return false;
    }
    s_spiBus[spi_bus].m_currDev = -1;
    mutex_unlock(s_spiBus[spi_bus].m_mutex);
    return true;
}

uint8 spi_soc_write(uint8 spi_dev, uint8 byte)
{
    uint8 spi_bus;
    if(spi_dev>=SPI_DEV_MAX)
    {
        TRACE_ERR("Invalid spi dev:%d",spi_dev);
        return 0;
    }
    OS_ENTER_CRITICAL();
    spi_bus = s_spiDev[spi_dev].m_spiBus;
    spi_soc_set_dev(spi_dev,true);
    /* 检查指定的SPI标志位设置与否:发送缓存空标志位 */
    WAIT_ON(SPI_I2S_GetFlagStatus(get_spi(spi_bus), SPI_I2S_FLAG_TXE)==RESET, SPI_RW_TIMEOUT);
    
    /* 通过外设SPIx发送一个数据 */
    SPI_I2S_SendData(get_spi(spi_bus), byte);
    WAIT_ON(SPI_I2S_GetFlagStatus(get_spi(spi_bus), SPI_I2S_FLAG_RXNE)==RESET, SPI_RW_TIMEOUT);
    SPI_I2S_ReceiveData(get_spi(spi_bus));
    spi_soc_set_dev(spi_dev,false);
    OS_EXIT_CRITICAL();
    return 1;
TIMEOUT:
    TRACE_ERR("spi_writex(0x%02x) timeout.",byte);
    spi_soc_set_dev(spi_dev,false);
    OS_EXIT_CRITICAL();
    return 0;
}

uint8 spi_soc_read(uint8 spi_dev, uint8* pByte)
{
    uint8 spi_bus;
    if(spi_dev>=SPI_DEV_MAX)
    {
        TRACE_ERR("Invalid spi dev:%d",spi_dev);
        return 0;
    }
    OS_ENTER_CRITICAL();
    spi_bus = s_spiDev[spi_dev].m_spiBus;
    spi_soc_set_dev(spi_dev,true);
    /* 检查指定的SPI标志位设置与否:发送缓存空标志位 */
    WAIT_ON(SPI_I2S_GetFlagStatus(get_spi(spi_bus), SPI_I2S_FLAG_TXE)==RESET,SPI_RW_TIMEOUT);

    /* 通过外设SPIx发送一个数据 */
    SPI_I2S_SendData(get_spi(spi_bus), 0xFF);
    WAIT_ON(SPI_I2S_GetFlagStatus(get_spi(spi_bus), SPI_I2S_FLAG_RXNE)==RESET,SPI_RW_TIMEOUT);
    *pByte = SPI_I2S_ReceiveData(get_spi(spi_bus));
    spi_soc_set_dev(spi_dev,false);
    OS_EXIT_CRITICAL();
    return 1;
TIMEOUT:
    TRACE_ERR("spi_read byte timeout.");
    spi_soc_set_dev(spi_dev,false);
    OS_EXIT_CRITICAL();
    return 0;
}

/* 有些器件使用CS来判断是否读写完毕 */
void spi_soc_xfer_start(uint8 spi_dev)
{
    spi_soc_set_dev(spi_dev,true);
}

/* 有些器件使用CS来判断是否读写完毕 */
void spi_soc_xfer_stop(uint8 spi_dev)
{
    spi_soc_set_dev(spi_dev,false);
}

#endif

