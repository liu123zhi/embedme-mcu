/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __UART_H__
#define __UART_H__

#include "basetype.h"

#define USE_UART_TX_INT   0   /* 是否使用中断方式发送,默认为查询方式 */

typedef enum
{
    UART_COM0=0,
    UART_COM1,
    UART_COM2,
    UART_COM3,
    UART_COM4
}UART_ID;

/* 串口波特率值 */
typedef enum
{
    B_1200      = 1200,
    B_2400      = 2400,
    B_4800      = 4800,
    B_9600      = 9600,
    B_19200     = 19200,
    B_38400     = 38400,
    B_57600     = 57600,
    B_115200    = 115200,
    B_460800    = 460800
}BAUDRATE_E;

/* 串口数据位值 */
typedef enum
{
    DATA_5=5,
    DATA_6,
    DATA_7,
    DATA_8
}DATABITS_E;

/* 串口停止位值 */
typedef enum
{
    STOP_1=1,
    STOP_2
}STOPBITS_E;

/* 串口校验位值 */
typedef enum
{
    PARITY_NONE=0,
    PARITY_ODD,
    PARITY_EVEN,
    PARITY_SPACE
}PARITY_E;

/* 串口流控位值 */
typedef enum{
    FLOWCTRL_NONE=0,
    FLOWCTRL_HW,
    FLOWCTRL_SW
}FLOWCTRL_E;

/*****************************************************
 * 串口发送方式有两种
 * 1.查询方式:阻塞方式查询发送状态寄存器,由于需要设置
 *   超时,数据有可能发送失败.
 * 2.中断方式:先将要发送的数据放到fifo中,空闲时再发送,
 *   确保了数据不会丢失,但需要一定的内存空间来保存数据.
 *   采用中断方式发送时,需要建立发送缓冲区及发送任务.
 * 本驱动同时支持两种方式,但使用的API不一样:
 * 查询方式API:
 * uart_send_byte()
 * uart_send_str()
 * uart_send_send()
 * 中断方式API:
 * uart_send_str_txint()
 * uart_send_send_txint()
 *****************************************************/

bool uart_init(uint8 uart, uint32 baudrate);
void uart_reset(uint32 uart);
uint32 uart_recv_byte(uint8 uart,char* ch);
uint32 uart_recv_line(uint8 uart, char* line,uint32 size);
uint32 uart_recv(uint8 uart, char* buf, uint32 size);
bool uart_send_byte(uint8 uart,char ch);
uint32 uart_send_str(uint8 uart, char* str);
uint32 uart_send(uint8 uart, char* data, uint32 len);
#if USE_UART_TX_INT
void task_uart_tx_polling(void* pdata);
#endif

#endif
