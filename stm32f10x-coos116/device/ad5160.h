/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __AD5160_H__
#define __AD5160_H__

#include "basetype.h"
typedef enum{
    AD5160_VOLUME_LVL0=0,
    AD5160_VOLUME_LVL1,
    AD5160_VOLUME_LVL2,
    AD5160_VOLUME_LVL3,
    AD5160_VOLUME_LVL4,
    AD5160_VOLUME_DEFAULT = AD5160_VOLUME_LVL4,
    AD5160_VOLUME_LVL5,
    AD5160_VOLUME_LVL6,
    AD5160_VOLUME_LVL7,
    AD5160_VOLUME_LVL8,
    AD5160_VOLUME_LVL9,
    AD5160_VOLUME_MAX
}VOLUME_LVL_E;

void ad5160_init();
sint8 ad5160_get_volume();
void ad5160_set_volume(sint8 level);
void ad5160_volume_up();
void ad5160_volume_down();

#endif
