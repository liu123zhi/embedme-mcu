/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "i2c.h"
#include "board.h"
#include "gpio.h"
#include "trace.h"
#include "kernel.h"

#define TIME_KP             2               /* 保持时间 */
#define TIME_WAIT_ACK       500             /* 等待回应超时时间 */
#define I2C_ADDR_RD(dev)    ((dev)|0x01)    /* 读时地址 */
#define I2C_ADDR_WR(dev)    ((dev)&0xFE)    /* 写时地址 */

/* I2C总线GPIO引脚定义,根据硬件设计更改 */
static I2cBus_S s_i2cBus[I2C_BUS_MAX]={
      /* SCL  SDA  initFlag */
      {PB(6),PB(7),0},
      {0    ,0    ,0}
};

static void i2c_set_scl(uint8 i2c_bus, uint8 val)
{
    GPIO_SET_VAL(s_i2cBus[i2c_bus].m_gpioSCL, !!val);
}

static void i2c_set_sda(uint8 i2c_bus, uint8 val)
{
    GPIO_SET_VAL(s_i2cBus[i2c_bus].m_gpioSDA, !!val);
}

static uint8 i2c_get_databit(uint8 i2c_bus)
{
    return GPIO_GET_VAL(s_i2cBus[i2c_bus].m_gpioSDA);
}

static void i2c_set_dir(uint8 i2c_bus, uint8 dir)
{
    if(dir==DIR_IN)
    {
        GPIO_INIT(s_i2cBus[i2c_bus].m_gpioSDA,GPIO_Mode_IN_FLOATING,GPIO_SPEED_50MHZ);
    }
    else
    {
        GPIO_INIT(s_i2cBus[i2c_bus].m_gpioSDA,GPIO_MODE_OUT_OD,GPIO_SPEED_50MHZ);
    }
}

/* I2C开始信号:SCL高电平期间,SDA由高变低 */
static void i2c_start(uint8 i2c_bus)
{   
    switch(i2c_bus){
    case I2C_BUS_0:
    case I2C_BUS_1:
        i2c_set_sda(i2c_bus,1);
        i2c_set_scl(i2c_bus,1);
        udelay(TIME_KP);
        i2c_set_sda(i2c_bus,0);
        udelay(TIME_KP);
        i2c_set_scl(i2c_bus,0);
        break;
    default:
        break;
    }
}
/* I2C停止信号:SCL高电平期间,SDA由低变高 */
static void i2c_stop(uint8 i2c_bus)
{
    switch(i2c_bus){
    case I2C_BUS_0:
    case I2C_BUS_1:
        i2c_set_scl(i2c_bus,0);
        i2c_set_sda(i2c_bus,0);
        udelay(TIME_KP);
        i2c_set_scl(i2c_bus,1);
        //udelay(TIME_KP);
        i2c_set_sda(i2c_bus,1);
        udelay(TIME_KP);
        break;
    default:
        break;
    }
}

/*********************************************
 * I2C主机应答信号:
 * SDA为低电平,表示主机接收字节完毕
 * SDA为高电平,表示主机结束接收从机数据
 */
static void i2c_ack(uint8 i2c_bus)
{
    switch(i2c_bus){
    case I2C_BUS_0:
    case I2C_BUS_1:
        i2c_set_scl(i2c_bus,0);
        i2c_set_sda(i2c_bus,0);
        udelay(TIME_KP);
        i2c_set_scl(i2c_bus,1);
        udelay(TIME_KP);
        i2c_set_scl(i2c_bus,0);
        break;
    default:
        break;
    }
}

static void i2c_nack(uint8 i2c_bus)
{
    switch(i2c_bus){
    case I2C_BUS_0:
    case I2C_BUS_1:
        i2c_set_scl(i2c_bus,0);
        i2c_set_sda(i2c_bus,1);
        udelay(TIME_KP);
        i2c_set_scl(i2c_bus,1);
        udelay(TIME_KP);
        i2c_set_scl(i2c_bus,0);
        break;
    default:
        break;
    }
}

/* I2C主机等待ACK信号:等待SDA线被拉低 */
static bool i2c_wait_ack(uint8 i2c_bus)
{
    uint8 timeout=0;
    switch(i2c_bus){
    case I2C_BUS_0:
    case I2C_BUS_1:        
    {
        i2c_set_sda(i2c_bus,1);
        udelay(TIME_KP);
        i2c_set_scl(i2c_bus,1);
        udelay(TIME_KP);
        i2c_set_dir(i2c_bus,DIR_IN);
        while(i2c_get_databit(i2c_bus))
    	{
    		timeout++;
    		if(timeout>TIME_WAIT_ACK)
    		{
                TRACE_ERR("i2c bus(%d) wait ack timeout.",i2c_bus);
                i2c_set_dir(i2c_bus,DIR_OUT);
                i2c_stop(i2c_bus);
    			return true;
    		}
			udelay(1);	 
    	}
    	i2c_set_scl(i2c_bus,0);
        i2c_set_dir(i2c_bus,DIR_OUT);
    	break;
    }
    default:
        break;
    }
    return false;
}
/* I2C 读取一个字节 */
static uint8 i2c_read_byte(uint8 i2c_bus)
{    
    uint8 i=0,receive=0;
    switch(i2c_bus){
    case I2C_BUS_0:
    case I2C_BUS_1: 
        i2c_set_scl(i2c_bus,0);/* 读前拉低SCL,为产生时序做准备 */
        udelay(TIME_KP);
        i2c_set_sda(i2c_bus,1);
        udelay(TIME_KP);
        i2c_set_dir(i2c_bus,DIR_IN);
        for(i=0;i<8;i++)
        {
            i2c_set_scl(i2c_bus,1);
            udelay(TIME_KP);
            receive<<=1;
            if(i2c_get_databit(i2c_bus))
            {
                receive++;
            }
            i2c_set_scl(i2c_bus,0);/* 读完后拉低SCL */
            udelay(TIME_KP);	
        }
        i2c_set_dir(i2c_bus,DIR_OUT);
        break;
    default:
        break;
    }
    return receive;
}

/* I2C 发送一个字节 */
static void i2c_write_byte(uint8 i2c_bus,uint8 byte)
{   
	uint8 i=0;
    switch(i2c_bus){
    case I2C_BUS_0:
    case I2C_BUS_1:
        for(i=0;i<8;i++)
    	{
            i2c_set_scl(i2c_bus,0);/* 写前拉低SCL,为产生时序做准备 */
            udelay(TIME_KP);
    		if((byte&0x80)>0)
    	    {
                i2c_set_sda(i2c_bus,1);
    		}
    		else
    		{
                i2c_set_sda(i2c_bus,0);
    		}
    		byte<<=1;
            udelay(TIME_KP);
    		i2c_set_scl(i2c_bus,1);
    		udelay(TIME_KP);
    	}
        i2c_set_scl(i2c_bus,0);
        udelay(TIME_KP);
        i2c_set_sda(i2c_bus,1);
        udelay(TIME_KP);
        break;
    default:
        break;
    }
}

/* 初始化i2c总线 */
bool i2c_gpio_init(uint8 i2c_bus,uint8 i2c_speed)
{
	UNUSED_PARAM(i2c_speed);
	switch(i2c_bus){
    case I2C_BUS_0:
    case I2C_BUS_1:
        if(s_i2cBus[i2c_bus].m_gpioSCL==0 || s_i2cBus[i2c_bus].m_gpioSDA==0)
        {
            TRACE_ERR("invalid i2c gpio, i2cbus:%d",i2c_bus);
            return false;
        }
        GPIO_INIT(s_i2cBus[i2c_bus].m_gpioSCL,GPIO_MODE_OUT_PP,GPIO_SPEED_50MHZ);
        i2c_set_dir(i2c_bus,DIR_OUT);
        i2c_set_scl(i2c_bus,1);
        i2c_set_sda(i2c_bus,1);
        break;
    default:
        return false;
    }
    TRACE_REL("Init gpio i2cbus:%d ok.",i2c_bus);
    return true;
}

uint32 i2c_gpio_read(uint8 i2c_bus,uint8 i2c_dev, uint8* wdata, uint32 wlen, uint8* rdata,  uint32 rlen)
{
    uint32 i=0;
    OS_ENTER_CRITICAL();
    if (wlen>0)/* 要先写数据 */
    {
        i2c_start(i2c_bus);
        i2c_write_byte(i2c_bus,I2C_ADDR_WR(i2c_dev));/* 写地址 */
        i2c_wait_ack(i2c_bus);
        for(i=0;i<wlen;i++)
        {
            i2c_write_byte(i2c_bus,*(wdata+i));
            i2c_wait_ack(i2c_bus);
        }
    }

    i2c_start(i2c_bus);
    i2c_write_byte(i2c_bus,I2C_ADDR_RD(i2c_dev));
    i2c_wait_ack(i2c_bus);
    i=0;
    while(rlen>0)
    {
        rdata[i++]=i2c_read_byte(i2c_bus);
        if (rlen==1)
        {
            i2c_nack(i2c_bus);
        }
        else
        {
            i2c_ack(i2c_bus);
        }
        rlen--;
    }
    i2c_stop(i2c_bus);
    OS_EXIT_CRITICAL();
    return i;
}

uint32 i2c_gpio_write(uint8 i2c_bus, uint8 i2c_dev, uint8 * data, uint32 len)
{
    uint32 i=0;
    OS_ENTER_CRITICAL();
    if(len>0)
    {
		i2c_start(i2c_bus);
	    i2c_write_byte(i2c_bus,I2C_ADDR_WR(i2c_dev));
	    i2c_wait_ack(i2c_bus);
	    for(i=0;i<len;i++)
	    {
	        i2c_write_byte(i2c_bus,*(data+i));
	        i2c_wait_ack(i2c_bus);
	    }
	    i2c_stop(i2c_bus);
	}
    OS_EXIT_CRITICAL();
    return i;
}

