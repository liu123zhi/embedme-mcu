/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "trace.h"
#include "system.h"
#include "board.h"
#include "config.h"
#include "kernel.h"
#include "rtc.h"
#include <string.h>

#define SHELL_CMD_LEN_MAX   64  /* shell命令最长为64字节 */
static char s_shellCmdBuf[SHELL_CMD_LEN_MAX];
/*******************************************************
 * msleep,sleep:
 * 睡眠函数,睡眠后会进行任务切换,每个Tick为10ms
 ******************************************************/
uint32 ms2ticks(uint32 ms)
{
    ms = MAX(SYSTEM_TICK_MS,ms);
    return ms/SYSTEM_TICK_MS;
}

void msleep(uint32 ms)
{
    uint32 ticks = ms2ticks(ms);
    CoTickDelay(ticks);
}

void sleep(uint32 sec)
{
    uint32 ticks = sec * SYSTEM_TICK_HZ;
    CoTickDelay(ticks);
}

/*******************************************************
 * system_core_dump:
 * 系统崩溃时处理
 ******************************************************/
void system_core_dump(OS_TID taskID)
{
    while(1)
    {
        TRACE_LEVEL(TRACE_LEVEL_DBG);
        TRACE_ERR("core dump at task:%d",taskID);
        sleep(1);
    }
}

/*******************************************************
 * system_reboot:
 * 系统重启
 ******************************************************/
void system_reboot(void)
{
    //platform_watchdog_init(1000);
    while(1);
}

/*******************************************************
 * system_frozen:
 * 系统冻结
 ******************************************************/
void system_frozen(char* reason)
{
    while(1)
    {
        TRACE_LEVEL(TRACE_LEVEL_DBG);
        TRACE_ERR("!!!system crash at: %s !!!---%s",reason,rtc_strtime());
        sleep(10);
    }
}


/*******************************************************
 * shell_handle_cmd:
 * 解析命令参数
 ******************************************************/
static uint8 system_shell_parse(char* cmd,char** argv,uint8 argvnum)
{
    uint8 i=0;
    uint8 start=0;
    uint8 argc=0;
    sint8 index=-1;
    uint8 cmdLen = strlen(cmd);
    for(i=0; i<cmdLen; i++)
    {
        if(cmd[i]==' ')
        {
            cmd[i]=0;
            if(i-start>0)
            {
                index++;
                if(index>=argvnum)
                    break;
                else
                    argv[index]=cmd+start;   
            }
            start=i+1;
        } 
    }
    /* 到达缓冲尾部 */
    if(start<i)
    {
        index++;
        if(index<argvnum)
            argv[index]=cmd+start; 
    }
    /* 计算参数个数 */
    for(i=0;i<argvnum;i++)
    {
        if(argv[i]!=NULL)
            argc++;
    }
    return argc;
}

/* 从终端获取用户输入 */
uint8 system_shell_read(uint8 uart, char** argv, uint8 argvnum,char ps1)
{
    uint8 i=0;
    uint8 argc=0;
    char* shellCmd = s_shellCmdBuf;
    memset(s_shellCmdBuf,0,SHELL_CMD_LEN_MAX);
    if (g_traceLevel==TRACE_LEVEL_DIS)
    {
        return 0;
    }
    if (ps1!=0)
    {
        uart_send_byte(uart, ps1);
    }
    while(1)
    {
        char ch=0;
        if(uart_recv_byte(uart, &ch)>0)
        {
            uart_send_byte(uart,ch);
            if(i<SHELL_CMD_LEN_MAX)
                shellCmd[i++]=ch;
            else
                ch = '\n';/* 到达最大长度,自动结束!!! */
            if(ch=='\r' || ch=='\n')/* 终端换行符可能只发一个'\r',不会发'\n' */
            {
                shellCmd[i-1]=0;
                if(strlen(shellCmd)>0)
                {
                    uart_send_byte(uart,'\r');
                    uart_send_byte(uart,'\n');
                    argc = system_shell_parse(shellCmd,argv,argvnum);
                    return argc;
                }
                else/* strlen(shellCmd)==0,只包含一个'\r'或'\n'的情况下,继续等待有效输入 */
                {
                    i=0;
                    memset(s_shellCmdBuf,0,SHELL_CMD_LEN_MAX);
                    uart_send_byte(uart,'\r');
                    uart_send_byte(uart,'\n');
                    continue;
                }
            }
        }
        msleep(10);
    }
}

void system_task_info(uint8 taskID, char* taskName)
{
    TRACE_ERR("Unsupport function: system_task_info.");
}

