/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __MY_TIMER_H__
#define __MY_TIMER_H__

#include "basetype.h"

/* 注意:
 * ucosii自带的定时器默认精度为100ms,也可以通过修改OS_TMR_CFG_TICKS_PER_SEC为100来实现定时精度10ms
 * 如果不想修改内核的话,则自己实现精度为10ms的定时器,付出的代价是增加执行文件的大小!
 */

#define USE_TIMER_BASED_ON_MYTASK  1    /* 是否使用自己实现的定时器(精度为10ms)*/

typedef enum{
    TIMER_TYPE_ONESHOT=0,
    TIMER_TYPE_PERIODIC,
    TIMER_TYPE_ABSTIME,     /* 绝对时间定时器 */
}TIMER_TYPE_E;

#define TIMER_ABS_TIME(hour,min,sec)  ((hour<<12)+(min<<6)+sec)

typedef void (*SEL_ontimer)(sint8 timerID);
/* SEL_ontimer为定时器回调函数,注意此函数要尽快退出,不能执行耗时或阻塞式的操作，否则会严重影响定时器精度!!! */
sint8 timer_create(uint8 timerType,uint32 ms_abs,SEL_ontimer ontimer);
void timer_detroy(sint8 timerID);
bool timer_start(sint8 timerID);
void timer_stop(sint8 timerID);
void task_timer_do_tick(void* pData);
#endif
