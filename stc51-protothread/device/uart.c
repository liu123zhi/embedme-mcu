/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "basetype.h"
#include "trace.h"
#include "platform.h"
#include "uart.h"

#define RXBUF_SIZE          8   /* 设置接收缓存大小为8字节 */
typedef struct{
    sint8 m_rx_iput;
    sint8 m_rx_iget;
    uint8 m_rx_max;
    uint8 m_rx_count;
}UartInfo_S;

static UartInfo_S s_uartInfo[UART_COUNT];
static char s_rxbuf[RXBUF_SIZE];

/* 中断服务程序 */
void isr_uart0(void) interrupt 4 using 2
{
    if (RI)
    {
        RI = 0;
        if(s_uartInfo[0].m_rx_count != s_uartInfo[0].m_rx_max)
        {
            s_uartInfo[0].m_rx_iput = (s_uartInfo[0].m_rx_iput+1) % s_uartInfo[0].m_rx_max;
            s_rxbuf[s_uartInfo[0].m_rx_iput]=SBUF;
            s_uartInfo[0].m_rx_count++;
        }
    }
}

bool uart_init(uint8 uart)
{
    if (uart>=UART_COUNT)
    {
        return false;
    }
    s_uartInfo[uart].m_rx_iget = -1;
    s_uartInfo[uart].m_rx_iput = -1;
    s_uartInfo[uart].m_rx_max = RXBUF_SIZE;
    s_uartInfo[uart].m_rx_count = 0;

    SCON = 0x50;    /* 模式1,8bit数据位,使能接收 */
    TMOD |= 0x20;   /* timer1,mode 2,8-bit reload */
    TH1 = 0xFD;     /* 11.0592M时钟下,9600波特率 */
    TL1 = 0xFD;     /* 重装值=TH1 */
    TR1 = 1;        /* timer1 run */
    EA = 1;         /* 开中断 */
    ES = 1;         /* 使能串口中断 */
    TI = 0;         /* 清除发送中断 */
    return true;
}

bool uart_recv_byte(uint8 uart,char* ch)
{
    if (uart>=UART_COUNT)
    {
        return false;
    }
    if(0==s_uartInfo[uart].m_rx_count)
    {
        return false;
    }
    s_uartInfo[uart].m_rx_iget = (s_uartInfo[uart].m_rx_iget+1) % s_uartInfo[uart].m_rx_max;
    *ch = s_rxbuf[s_uartInfo[uart].m_rx_iget];
    s_uartInfo[uart].m_rx_count--;
    return true;
}

bool uart_send_byte(uint8 uart, char ch)
{
    if (uart>=UART_COUNT)
    {
        return false;
    }
    SBUF = ch;
    while(TI!=1);/* 等待发送完毕 */
    TI=0;
    return true;
}

void uart_send_str(uint8 uart,const char* str)
{
    char* pchr=(char*)str;
    while(*pchr)
    {
        uart_send_byte(uart, *pchr);
        pchr++;
    }
}

void uart_format_value(uint8 uart,uint8 value,uint8 radix)
{
    char chr;
    if (radix==16)
    {
        uart_send_byte(uart, '0');
        uart_send_byte(uart, 'x');
        chr=DIGIT2ASCII((value&0xF0)>>4);
        uart_send_byte(uart, chr);
    
        chr=DIGIT2ASCII(value&0x0F);
        uart_send_byte(uart, chr);
    }
    else if (radix==10)
    {
        chr=DIGIT2ASCII(value/100);
        if(chr!='0')
        {
            uart_send_byte(uart, chr);
            value = value%100;
            chr=DIGIT2ASCII(value/10);
            uart_send_byte(uart, chr);
       }
       else
       {
            value = value%100;
            chr=DIGIT2ASCII(value/10);
            if(chr!='0')
            {
               uart_send_byte(uart, chr);
            }
       }
       chr=DIGIT2ASCII(value%10);
       uart_send_byte(uart, chr);
       
    }
}


