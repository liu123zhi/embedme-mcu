/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "board.h"
#include "basetype.h"
#include "gpio.h"

void hjboard_set_alarm(bool enable)
{
    if (enable)
    {
        GPIO_FM = 0;
    }
    else
    {
        GPIO_FM = 1;
    }
}

void hjboard_set_i2c(bool enable)
{
    /* RTC ds1302与I2C共用了两根线,如果要使用I2C必须将其禁止掉 */
    if(enable)
    {
        GPIO_SET_VAL(RTC_RST,0);
    }
}

void board_init()
{
    /* 关闭数码管显示 */
    GPIO_SET_VAL(GPIO_DU,0);
    GPIO_SET_VAL(GPIO_WE,0);
}