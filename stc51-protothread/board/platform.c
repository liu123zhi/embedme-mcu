/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "platform.h"
static uint8 s_t0h_reloader=0x00;
static uint8 s_t0l_reloader=0x00;
static uint32 s_system_ticks=0;
void mdelay(uint32 ms)
{
    while(ms--)
    {
        uint32 mips=CPU_MIPMS;
        while(mips--);
    }
}
void udelay(uint32 us)
{
    while(us--)
    {
        uint32 mips=CPU_MIPUS;
        while(mips--);
    }
}

void ticktimer_set(TickTimer_S* timer,uint32 interval)
{
    timer->m_start = s_system_ticks; 
    timer->m_interval = interval; 
}

bool ticktimer_expired(TickTimer_S* timer)
{
    return ((s_system_ticks - timer->m_start)>=timer->m_interval); 
}


uint32 platform_get_ticks(void)
{
    return s_system_ticks;
}

static void platform_set_systick(uint8 tick_ms)
{
    /* 11.0592M晶振时,12分频后为921600Hz,则10ms为9216个周期
     * 因此初值为65536-9216=56320(0xDC00) */
    uint16 timer_reloader;
    switch(tick_ms){
    case 50:
    case 20:
        break;
    default:
        tick_ms = 10;
        break;
    }
    timer_reloader = (65536-9216*(tick_ms/10));
    s_t0h_reloader = timer_reloader/256;
    s_t0l_reloader = timer_reloader%256;
    TMOD |= 0x01;   /* 选择模式1,16位定时器 */
    TH0   = s_t0h_reloader;
    TL0   = s_t0l_reloader;
    EA    = 1;      /* 开总中断 */
    ET0   = 1;      /* 定时器中断打开 */
    TR0   = 1;      /* 启动定时器 */
}

/* 中断处理函数 */
void isr_timer0(void) interrupt 1 using 1
{
    TH0 = s_t0h_reloader;
    TL0 = s_t0l_reloader;
    ET0 = 0;
    s_system_ticks++;
    system_time_tick();
    ET0 = 1;
}

void platform_init(void)
{
    platform_set_systick(SYSTEM_TICK_MS);
}


