/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "basetype.h"
#include "trace.h"
#include "board.h"
#include "kernel.h"
#include "mytask.h"

/*******************************************************
 * main:
 * 主程序
 ******************************************************/
int main(void)
{
    OSInit();
    TRACE_LEVEL(TRACE_LEVEL_DIS);/* 默认关闭调试信息 */
    platform_init();/* soc级别的初始化 */    
    OSTaskCreateExt(task_main,
                    (void *)0,
                    (OS_STK *)&stack_task_main[TASK_STACK_1024B-1],
                    TASK_PRIORITY_MAIN,
                    TASK_PRIORITY_MAIN,
                    (OS_STK *)&stack_task_main[0],
                    TASK_STACK_1024B,
                    (void *)0,
                    OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR);    
    OSTimeSet(0);
    OSStart();
    return 0;
}
