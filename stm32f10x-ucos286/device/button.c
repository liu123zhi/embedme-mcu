/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "kernel.h"
#include "board.h"
#include "gpio.h"
#include "button.h"
#include "trace.h"

typedef enum{
    BTN_STATE_IDLE=0,
    BTN_STATE_PRESSED,
    BTN_STATE_RELEASED
}BTN_STATE_E;

typedef struct{
    uint8 m_id;
    uint8 m_state;
    uint8 m_count;
    uint8 m_lastPinVal:4;
    uint8 m_pressedPinVal:4;
    uint32 m_gpio;
}BtnInfo_S;

static BtnInfo_S s_buttonInfo[BTN_ID_MAX]={0};
bool button_init(uint8 buttonID, GpioInfo_S gpioInfo)
{
    if (buttonID>=0 && buttonID<BTN_ID_MAX)
    {
        s_buttonInfo[buttonID].m_id=buttonID;
        s_buttonInfo[buttonID].m_state=BTN_STATE_IDLE;
        s_buttonInfo[buttonID].m_count=0;
        s_buttonInfo[buttonID].m_gpio=gpioInfo.m_gpio;
        s_buttonInfo[buttonID].m_pressedPinVal=gpioInfo.m_enVal;
        s_buttonInfo[buttonID].m_lastPinVal=GPIO_GET_VAL(s_buttonInfo[buttonID].m_gpio);
        return true;
    }
    TRACE_ERR("button[%d] init error!",buttonID);
    return false;
}

void button_scan(OS_EVENT* evtMsgQueue)
{
    uint8 i;
    if (evtMsgQueue==NULL)
    {
        return;
    }
    for(i=0;i<BTN_ID_MAX;i++)
    {
        uint8 pinVal;
        if (s_buttonInfo[i].m_id!=i)
        {
            return;
        }
        pinVal=GPIO_GET_VAL(s_buttonInfo[i].m_gpio);
        if(pinVal!=s_buttonInfo[i].m_lastPinVal)
        {
            s_buttonInfo[i].m_count=0;
            if(pinVal==s_buttonInfo[i].m_pressedPinVal)
            {
                s_buttonInfo[i].m_state=BTN_STATE_PRESSED;
            }
            else
            {
                s_buttonInfo[i].m_state=BTN_STATE_RELEASED;   
            }
            s_buttonInfo[i].m_lastPinVal=pinVal;
            continue;
        }
        else
        {
            if (s_buttonInfo[i].m_state!=BTN_STATE_IDLE)
            {
                s_buttonInfo[i].m_count++;
                if (s_buttonInfo[i].m_count>=2)/* 必须维持2个tick以上(20ms),才算有效 */
                {
                    ButtonEvent_S* btnEvt=malloc(sizeof(ButtonEvent_S));
                    btnEvt->m_id = s_buttonInfo[i].m_id;
                    if(s_buttonInfo[i].m_state==BTN_STATE_PRESSED)
                    {
                        btnEvt->m_evt = BTN_EVT_DOWN;
                    }
                    else
                    {
                        btnEvt->m_evt = BTN_EVT_UP;
                    }
                    if(OS_ERR_NONE!=OSQPost(evtMsgQueue, (void*)btnEvt))
                    {
                        TRACE_DBG("send button[%d] event error.",btnEvt->m_id);
                    }
                    s_buttonInfo[i].m_count=0;
                    s_buttonInfo[i].m_state=BTN_STATE_IDLE;
                }   
            }
            else
            {
                s_buttonInfo[i].m_count=0;
                s_buttonInfo[i].m_state=BTN_STATE_IDLE;
            }
            
        }
    }
}

