/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __GPIO_H__
#define __GPIO_H__
/**********************************************************
 File   : gpio.h
 Bref   : GPIO接口头文件 
 Version: V1.0.0
 Author : FergusZeng
 Date   : 2013-12-08
***********************************************************/
#include "basetype.h"
#include "stm32f10x.h"

#define PA(pin) (GPIOA_BASE | (pin))
#define PB(pin) (GPIOB_BASE | (pin))
#define PC(pin) (GPIOC_BASE | (pin))
#define PD(pin) (GPIOD_BASE | (pin))
#define PE(pin) (GPIOE_BASE | (pin))
#define PF(pin) (GPIOF_BASE | (pin))
#define PG(pin) (GPIOG_BASE | (pin))

#define GPIO_LOW  0     /* 低电平 */
#define GPIO_HIGH 1     /* 高电平 */

typedef enum{
    DIR_IN=0,
    DIR_OUT=1,
}DIR_E;

/* 为兼容重新定义GPIOMode_TypeDef */
typedef enum
{ GPIO_MODE_AIN = 0x0,          /* 模拟输入 */
  GPIO_MODE_IN_FLOATING = 0x04, /* 浮空输入 */
  GPIO_MODE_IPD = 0x28,         /* 下拉输入 */
  GPIO_MODE_IPU = 0x48,         /* 上拉输入 */
  GPIO_MODE_OUT_OD = 0x14,      /* 开漏输出 */
  GPIO_MODE_OUT_PP = 0x10,      /* 推挽输出 */
  GPIO_MODE_AF_OD = 0x1C,       /* 复用开漏输出 */
  GPIO_MODE_AF_PP = 0x18        /* 复用推挽输出 */
}GPIO_MODE_E;

/* 为兼容重新定义GPIOSpeed_TypeDef */
typedef enum
{ 
  GPIO_SPEED_10MHZ = 1, /* 最大速率为10M */
  GPIO_SPEED_2MHZ,      /* 最大速率为2M */
  GPIO_SPEED_50MHZ      /* 最大速率为50M */
}GPIO_SPEED_E;

typedef enum{
    GPIO_VALUE_LOW=0,
    GPIO_VALUE_HIGH
}GPIO_VALUE_E;

typedef struct{
    uint32 m_gpio;
    uint8 m_mode;
    uint8 m_speed;
    uint8 m_enVal;
}GpioInfo_S;

#define GPIO_PORT(gpio)                ((GPIO_TypeDef*)((gpio)&0xFFFFFFF0))
#define GPIO_PIN(gpio)                  ((uint16)(1 << (((uint8)(gpio))&0x0F)))
#define GPIO_INIT(gpio, mode, speed)    {GPIO_InitTypeDef  gpinit; \
                                        gpinit.GPIO_Pin   = GPIO_PIN(gpio);\
                                        gpinit.GPIO_Speed = speed;\
                                        gpinit.GPIO_Mode  = mode;\
                                        GPIO_Init(GPIO_PORT(gpio), &gpinit);}
#define GPIO_GET_VAL(gpio)      GPIO_ReadInputDataBit(GPIO_PORT(gpio), GPIO_PIN(gpio))
#define GPIO_SET_VAL(gpio,val)  {if(!!val){GPIO_SetBits(GPIO_PORT(gpio), GPIO_PIN(gpio));}else{GPIO_ResetBits(GPIO_PORT(gpio), GPIO_PIN(gpio));}}

#define GPIO_WRITE_BYTE(gpio,offset,byte)  GPIO_Write(GPIO_PORT(gpio), (byte<<(offset)))
#define GPIO_READ_BYTE(gpio,offset)        (((GPIO_ReadInputData(GPIO_PORT(gpio)))&(0xFF<<(offset)))>>(offset))


#endif

