app              应用程序代码

board            板子相关代码
  |---board.h    板子公用接口

common           公用功能代码

device           外设驱动代码

kernel           与硬件体系无关的内核代码,可移植性非常强

lib              STM32标准库源码

portable         移植时需要修改的文件