/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "kernel.h"
#include "fifo.h"
#include <string.h>

#ifndef OS_ENTER_CRITICAL
#define OS_ENTER_CRITICAL()
#endif

#ifndef OS_EXIT_CRITICAL
#define OS_EXIT_CRITICAL()
#endif

bool fifo_init(void *pfifo, uint32 size)
{
    fifo_s *fifo;
    if (sizeof(fifo_s) >= size)
    {
        return false;
    }
    OS_ENTER_CRITICAL();
    fifo 		= (fifo_s *)pfifo;
    fifo->start = (char*)((char*)pfifo + sizeof(fifo_s));
    fifo->end   = (char*)((char*)pfifo + size);
    fifo->in    = fifo->start;
    fifo->out   = fifo->start;
    fifo->size  = (uint32)(fifo->end - fifo->start);
    fifo->entry = 0;
    OS_EXIT_CRITICAL();
    return true;
}

bool fifo_putchar(void *pfifo, char ch)
{
    fifo_s *fifo;
	OS_ENTER_CRITICAL();
    fifo = (fifo_s *)pfifo;
    if (fifo->entry < fifo->size)
    {
        *fifo->in++ = ch;
        fifo->entry++;
        if (fifo->in == fifo->end)
        {
            fifo->in = fifo->start;
        }
        OS_EXIT_CRITICAL();
        return true;
    }
	OS_EXIT_CRITICAL();
    return false;
}


uint32 fifo_putbuf(void *pfifo, char *buf, uint32 len)
{
    fifo_s *fifo;
    uint32 i;
	OS_ENTER_CRITICAL();
    fifo = (fifo_s *)pfifo;
    for(i=0; i<len; i++)
    {
        if (fifo->entry < fifo->size)
        {
            fifo->entry++;
            *fifo->in++ = *(buf+i);
            if (fifo->in == fifo->end)
            {
                fifo->in = fifo->start;
            }
        }
        else
        {
			OS_EXIT_CRITICAL();
            return i;
        }
    }
	OS_EXIT_CRITICAL();
    return i;
}


uint32 fifo_putstr(void *pfifo, char *pstr)
{
    fifo_s *fifo;
	OS_ENTER_CRITICAL();
    fifo = (fifo_s *)pfifo;
	if ((fifo->size - fifo->entry) < strlen((char*)pstr))
	{
		OS_EXIT_CRITICAL();
		return 0;
	}
    while (*pstr)
    {
        *fifo->in++ = *pstr++;
        fifo->entry++;
        if (fifo->in == fifo->end)
        {
            fifo->in = fifo->start;
        }
    }
	OS_EXIT_CRITICAL();
    return strlen(pstr);
}


uint32 fifo_getchar(void *pfifo, char* pch)
{
    fifo_s *fifo;
	OS_ENTER_CRITICAL();
    fifo = (fifo_s *)pfifo;
    if (fifo->entry > 0)
    {
        *pch = *fifo->out++;
        fifo->entry--;
        if (fifo->out == fifo->end)
        {
            fifo->out = fifo->start;
        }
        OS_EXIT_CRITICAL();
        return 1;
    }
	OS_EXIT_CRITICAL();
    return 0;
}

uint32 fifo_getbuf(void* pfifo, char* pbuf, uint32 len)
{
    uint32 pos=0;
    fifo_s *fifo;
	OS_ENTER_CRITICAL();
    fifo = (fifo_s *)pfifo;
    while (fifo->entry > 0 && pos<len)
    {
        fifo->entry--;
        *(pbuf+pos) = *fifo->out++;
        if (fifo->out == fifo->end)
        {
            fifo->out = fifo->start;
        }
        pos++;
    }
	OS_EXIT_CRITICAL();
    return pos;
}

uint32 fifo_getline(void *pfifo,char *pbuf,uint32 size)
{
    char ch=0;
    uint32 pos=0;
    fifo_s *fifo;
    fifo = (fifo_s *)pfifo;
    size--;
	OS_ENTER_CRITICAL();

    /* 首先要判断缓冲中是否有新行 */
    if(fifo->entry>0)
    {
        if((fifo->in-fifo->out)==fifo->entry)
        {
            ch =0;
            pos=0;
            while(ch==0)
            {
                if(fifo->out+pos==fifo->in) 
                {
                    break;
                } 
                else if(*(fifo->out+pos)=='\n')
                {
                    ch = 1;
                }
                pos++;
            }
        }
        else if((fifo->out-fifo->in)==(fifo->size-fifo->entry))
        {
            ch = 0;
            while(ch==0)
            {
                if(fifo->out+pos==fifo->end)
                {
                    break;
                }
                else if(*(fifo->out+pos)=='\n')
                {
                    ch = 1;
                }
                pos++;
            }
            pos=0;
            while(ch==0)
            {
                if(fifo->start+pos==fifo->in)
                {
                    break;
                }
                else if(*(fifo->start+pos)=='\n')
                {
                    ch = 1;
                }
				pos++;
            } 
        }
    }

    if(ch==0)
    {
        OS_EXIT_CRITICAL();
        return 0;
    }
    pos =0;
    while (fifo->entry > 0)
    {
        if(pos==size)
        {
            *(pbuf+pos) = 0; 
            OS_EXIT_CRITICAL();
            return strlen(pbuf);
        }
        fifo->entry--;
        ch = *fifo->out++;
        if (fifo->out == fifo->end)
        {
            fifo->out = fifo->start;
        }
        *(pbuf+pos) = ch;
        pos++;
        if (ch == '\n')
        {
            *(pbuf+pos) = 0;
			OS_EXIT_CRITICAL();
            return strlen(pbuf);
        }
    }
	OS_EXIT_CRITICAL();
    return 0;
}


void fifo_flush(void *pfifo)
{
    fifo_s *fifo;
	OS_ENTER_CRITICAL();
    fifo = (fifo_s *)pfifo;
    fifo->in = fifo->start;
    fifo->out = fifo->start;
    fifo->entry = 0;
    OS_EXIT_CRITICAL();
}

